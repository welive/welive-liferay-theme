/// GLOBAL VARIABLE DEFINITION ///

// Timer per il resize eseguito con il setTimeout()
var resizeTimer;
var ref = 0;
// Dimensione della window
var w = $(window).width();
// Variabile che monitora se il dropdown menu è aperto o chiuso
var isMenuOpen = false;

/// ***** FAVICON ***** ///
/*!
 * Cambia il Favicon dinamicamente
 * Works in all A-grade browsers except Safari and Internet Explorer
 */

document.head || (document.head = document.getElementsByTagName('head')[0]);

function changeFavicon(src) {
	 var link = document.createElement('link'),
	     oldLink = document.getElementById('dynamic-favicon');
	 
	 $( "link[rel='Shortcut Icon']" ).remove();
	 
	 link.id = 'dynamic-favicon';
	 link.rel = 'shortcut icon';
	 link.href = src;
	 if (oldLink) {
	  document.head.removeChild(oldLink);
	 }
	 document.head.appendChild(link);
}

/// ***** CSS LOADER ***** ///
/*!
 * Carica un CSS dinamicamente
 * Usage: loadCSS("/css/file.css");
 */
loadCSS = function(href) {

	  var cssLink = $("<link>");
	  $("head").append(cssLink); //IE hack: append before setting href

	  cssLink.attr({
	    rel:  "stylesheet",
	    type: "text/css",
	    href: href
	  });

};



AUI().ready(
	'liferay-hudcrumbs', 'liferay-navigation-interaction', 'liferay-sign-in-modal',
	function(A) {
		var navigation = A.one('#navigation');

		if (navigation) {
			navigation.plug(Liferay.NavigationInteraction);
		}

		var siteBreadcrumbs = A.one('#breadcrumbs');

		if (siteBreadcrumbs) {
			siteBreadcrumbs.plug(A.Hudcrumbs);
		}

		var signIn = A.one('li.sign-in a');

		if (signIn && signIn.getData('redirect') !== 'true') {
			signIn.plug(Liferay.SignInModal);
		}

		// Dockbar vertical JS, if dockbar is present
		var portletDockbar = $('#_145_dockbar');
		
		if (portletDockbar) {
			var body = $('.aui body');
//			var topbar = $('.aui body #topbar');
			
//			topbar.append('<div id="adminDockbar" class="adm-toggle-dockbar vertical-dockbar-close"><i class="shape-icon-toggle icon-cog"></i>'+Liferay.Language.get("theme.dockbar.admin")+'</div>');
			body.append('<div class="layer-mobile visible-phone vertical-dockbar-close"></div>');

			var toggleDockbar = $('.adm-toggle-dockbar');
			var toggleDockbarClose = $('.vertical-dockbar-close');
			var toggleDockbarIcon = $('.adm-toggle-dockbar .icon-cog');
			var contentMain = $('#wrapper #content');

			if (toggleDockbar) {
				toggleDockbarClose.on('click', toggleAdminDockbar);
				
				contentMain.on('click', function(){
					if($(".adm-toggle-dockbar.over").length > 0) {
						toggleAdminDockbar();
					}
				});
			}
		};
		function toggleAdminDockbar() {
			portletDockbar.toggleClass('over');
			toggleDockbar.toggleClass('over');
			toggleDockbarIcon.toggleClass('icon-remove');
			toggleDockbarIcon.toggleClass('icon-cog');
			body.toggleClass('lfr-has-dockbar-vertical');
		}
		/* Cambia il Favicon e i CSS MaterialDesign dinamicamente in base allo schema di tema scelto*/
		changeFavicon(themeCssPath+'/core/images/color_schemes/'+theme_selected_schema+'/favicon.ico');

		
		//Gestisce il logo del portale in seguito alle dimensioni della finestra
		manageLogo();
		//Gestisce il refresh della pagina al cambio di resize
		manageRefresh(w, ref);
		//Gestisce la label per il menu delle notifiche utente
		notificationLabel();
		//Cambia il colore dell'indicatore del Tab attivo
		$(".tabs div.indicator").addClass("core-color color-1");
		// Gestisce la posizione e l'apertura dei menu legati alla Dockbar Liferay
		resizeDropDownMenu();
	}
);



Liferay.Portlet.ready(

	/*
	This function gets loaded after each and every portlet on the page.

	portletId: the current portlet's id
	node: the Alloy Node object of the current portlet
	*/
	function(portletId, node) {

	}
);

Liferay.on(
	'allPortletsReady',

	/*
	This function gets loaded when everything, including the portlets, is on
	the page.
	*/

	function() {

	}
	
);

function notificationLabel(){
	if(w < 993){
		$("a.dropdown-toggle.user-notification-link").prepend('<span class="notificationText">'+Liferay.Language.get("theme.menu.notification")+'</span>');
	}
}


// Carica il Logo oppure il Logo Simple (senza scritta) in rapporto alla lunghezza della toolbar e dei suoi menu.
function manageLogo(){
//	var theme_name = "EssentialCoreMaterial_977-theme";
	
	var heading = $("#heading").outerWidth(true);
	var logo = $("#logo-container").outerWidth(true);
	var dockbar = $(".nav-wrapper ul").outerWidth(true);
	var win = $(window).width();
	heading = logo+dockbar;

	if(heading > 992){
	     if(dockbar > 800 && win < (heading) ){
	          $("#logo-img").attr("src", themeCssPath+"/core/images/color_schemes/"+theme_selected_schema+"/logo-simple.png");
	          $("#textPilot").addClass("simplePilot");
	     }else{
	          $("#logo-img").attr("src", themeCssPath+"/core/images/color_schemes/"+theme_selected_schema+"/logo.png");
	          $("#textPilot").removeClass("simplePilot");
	     }
	}else{
	     $("#logo-img").attr("src", themeCssPath+"/core/images/color_schemes/"+theme_selected_schema+"/logo.png");
	     $("#textPilot").removeClass("simplePilot");
	}
}

//Se al resize si cambia fascia di layout (mobile/desktop) viene ricaricata la pagina
function manageRefresh(wid, r){

	if(r==0){
		if(wid > 992) {ref=1;return;}
		if(wid <= 992) {ref=2; return;}
//		if(wid > 600 && wid <= 992) {ref=2; return;}
//		if(wid <= 600) {ref=3; return;}
		
	}else if (wid > 992 && r!=1) {
		ref=1;
		initDockbar();
	}else if (wid <= 992 && r!= 2) {
		ref=2;
		initDockbar();
	}else{
		return;
	}
}

/* Al resize della finestra avvia le funzioni necessarie a mantenere il portale responsive */
$(window).on('resize', function(e) {

	if ($(window).width()==w) {
		return;
	}
	else{
	  clearTimeout(resizeTimer);
	  resizeTimer = setTimeout(function() {
		  manageLogo();
		  shiftDockbar();//function definita in "navbar.vm"
		  manageRefresh(w, ref);
		  
	  }, 250);
	  w = $(window).width();
	}
	
});

/* Re-Inizializza la Dockbar di Liferay al passaggio desktop/mobile ed abilita il click sui menu dropdown */
function initDockbar(){
	isMenuOpen = false;
	
	AUI().use("liferay-dockbar", function(){ Liferay.Dockbar.init("#_145_dockbar"); });

	$(".nav-wrapper, #content, #sidenav-overlay").on("click", function(e){
		if(isMenuOpen && $(this)!==$(".dropdown-toggle")){
			$(".dropdown-toggle").parent().removeClass("open");
			isMenuOpen=!isMenuOpen;
		}
	});
	
	$(".dropdown-toggle").on("click", function(e){
		$(".dropdown-toggle").parent().removeClass("open");
		$(this).parent().toggleClass("open");	
		isMenuOpen=!isMenuOpen;
		e.stopPropagation();
	});
	
	notificationLabel();
	resizeDropDownMenu();
}


/* Gestisce la posizione e l'apertura dei menu legati alla Dockbar Liferay*/
function resizeDropDownMenu(){

	$(".user-avatar-link.dropdown-toggle").bind('click, tap', function(ev){
		
		var $this = $(this);
		var offset = $this.offset();
		var menu = $this.next(".dropdown-menu");
			menu.css('height', "64px");
		var h = 0;
		
		$(".user-avatar .dropdown-menu li").each(function( index ) {
			  h=h+$( this ).outerHeight();
		});
		menu.css('min-width', $this.outerWidth(true));
//		menu.css('width', $this.outerWidth(true));
		menu.animate({height: h}, 200, "linear");
	});

}
