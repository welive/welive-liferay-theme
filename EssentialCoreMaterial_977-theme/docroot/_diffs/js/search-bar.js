
(function() {

	var bodyEl = document.body,
		content = document.querySelector( '#content' ),

//		menu search
		opensearchbtn = $('.showHideSearch'),
		
		heading_height = $("#banner").outerHeight( true ),
		language_height = $("#languageDiv").outerHeight( true ),
		isSearchOpen = false;

	function init() {
		initEvents();
	}

	function initEvents() {
		
		if(opensearchbtn.length>0) opensearchbtn.on( 'click', toggleSearch );
		
		// close the menu element if the target it is not the menu element or one of its descendants...
		
		content.addEventListener( 'click', function(ev) {
			var target = ev.target;
			
			if( isSearchOpen && target !== opensearchbtn ) {
				toggleSearch();
			}
		});
	}
	
	function slideContent(menu, direction){
		
		var menuHeigth = $(menu).outerHeight( true );
		var yMenuPosition = heading_height + language_height;
		var slideContent = menuHeigth; 
		
		
		if(direction == "up") {
			yMenuPosition = yMenuPosition*-1;
			slideContent = slideContent-menuHeigth;
		}
		//Scorro Content e SearchDiv verso il basso o verso l'alto
		$("div#content").css("margin-top", slideContent+"px");
		$(menu).css("transform","translate3d(0px, "+yMenuPosition+"px, 0px)")
		
	}

	
	function toggleSearch() {

		if( isSearchOpen ) {
			$(".searchDiv").css({"z-index":"-1","opacity":"0"});
			slideContent(".searchDiv", "up");
			classie.remove( bodyEl, 'show-search');
			opensearchbtn.removeClass('pressed');
		}
		else {
			classie.add( bodyEl, 'show-search' );
			opensearchbtn.addClass('pressed');
			slideContent(".searchDiv", "down");

			$(".searchDiv").css({"z-index":"1","opacity":"1"});
		}
		isSearchOpen = !isSearchOpen;
	}


	init();

})();