

	(function($) {
		$(function() {
			$.cookieBar({
				expireDays: 30,
//				message: Liferay.Language.get("theme.cookie.policy.brief"),
				message: messageCookie,
//				acceptText: Liferay.Language.get("theme.cookie.policy.agree"),
				acceptText: acceptCookie,
				policyButton: true,
//				policyText: Liferay.Language.get("theme.cookie.policy"),
				policyText: policyCookie,
				policyURL: '#modal-cookie-policy'
			});
			$('a.cb-policy').click(function() {
				var jModalCookiePolicy = $('#modal-cookie-policy');
				if (jModalCookiePolicy.length > 0 && jModalCookiePolicy.css('display') === 'none') {
					jModalCookiePolicy.openModal();
				}
				return false;
			});

		}); // end of document ready

		$(window).load(function() {
			$('#modal-cookie-policy').appendTo($('nav').closest('.materialize'));
			
		}); // end of window load

	})(jQuery); // end of jQuery name space
