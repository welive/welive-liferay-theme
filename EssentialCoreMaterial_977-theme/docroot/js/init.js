// Web Storage check

function storageAvailable(type) {
	try {
		var storage = window[type],
			x = '__storage_test__';
		storage.setItem(x, x);
		storage.removeItem(x);
		return true;
	}
	catch(e) {
		return false;
	}
}



//MATERIALIZE activations
$(document).ready(function(){
	
  $('.button-collapse').sideNav({
      menuWidth: 260, // Default is 240
      edge: 'left', // Choose the horizontal origin
    }
  );
  
  $('.dropdown-button').dropdown({
  	constrain_width: false,
  	alignment: 'right'
  	});
  
  $('.modal-trigger-theme').leanModal();
  
  $('.collapsible').collapsible({
      accordion : true // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });
    $('.materialize select').material_select();
});


//Mappa la variabile di sessione che contiene il pilota con la stringa corretta da visualizzare
mapPilot = function(pilot){
	
	var mappedPilot = "";
	
	switch (pilot) {
	    case "Bilbao":
	    	mappedPilot = "Bilbao";
	        break;
	    case "Uusimaa":
	        mappedPilot = "Helsinki Region";
	        break;
	    case "Novisad":
	    	mappedPilot = "Novi Sad";
	        break;
	    case "Trento":
	    	mappedPilot = "Trento";
	        break;
	    default:
			console.log("No Pilot on Session");
	} 
	
	return mappedPilot;
}


//Appende il nome del Pilota inpostato nella "Web Storage" accanto al Logo.
//Visualizza o meno il menu dei Tools alla presenza del Pilota in sessione.
$(document).ready(function(){
	
	console.log("Init pilot "+sessionStorage.getItem('pilot')+"  Active: "+manage_pilot+" Schema: "+theme_selected_schema);
	//Mostro il menù dei tools se riesco a recuperare il pilota e funziona la WebStorage. 
 	if (storageAvailable('sessionStorage') && manage_pilot==true) {
 		if(sessionStorage.getItem('pilot') != null){
 			$("#logo-container").append("<b id='textPilot'>"+mapPilot(sessionStorage.getItem('pilot'))+"</b>");
 			$(".menuTools").css("display", "block");
 		}else{
 			$(".menuTools").css("display", "none");
 		}
 	}
 	else if(theme_selected_schema == "core-2"){ //Se non ho selezionato lo schema 2 del tema, mostro un alert se non riesco a recuperare il pilota oppure non funziona la WebStorage. 
 		// Too bad, no sessionStorage for us
 		console.log("In this browser session Storage is Deactivated or Manage Pilot is false.");
 		$('#modal_alert').openModal();
 	}

});


