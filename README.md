# README #

This project is a Liferay 6.2 theme

### What is this repository for? ###

Liferay Theme

## Requirements  
| Framework                                                                        |Version  |License             |Note 																	|
|--------------------------------------------------------------------------------------------|---------|--------------------|-------------------------------------------------------------------------|
|[Java SE Development Kit](https://www.java.com/it/download/help/index_installing.xml?j=7) |7.0 |[Oracle Binary Code License](http://www.oracle.com/technetwork/java/javase/terms/license/index.html)         |Also v.8|
|[Liferay Portal CE bundled with Tomcat](https://sourceforge.net/projects/lportal/files/Liferay%20Portal/6.2.3%20GA4/liferay-portal-tomcat-6.2-ce-ga4-20150416163831865.zip/download) |6.2 GA4 | [GNU LGPL](https://www.liferay.com/it/downloads/ce-license)      ||
|[postgreSQL](https://www.postgresql.org/download/)| 9.3  |[PostgreSQL License](https://www.postgresql.org/about/licence/) ||
|[Social Office CE](https://web.liferay.com/it/community/liferay-projects/liferay-social-office/overview) | 3.0 | GNU LGPL |

## Libraries

Liferay Theme is based on the following software libraries and frameworks.

|Framework                           |Version       |Licence                                      |
|------------------------------------|--------------|---------------------------------------------|
|[Apache Commons Logging](http://commons.apache.org/proper/commons-logging/) | 1.2 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[classie - class helper functions](https://github.com/ded/bonzo) |  | MIT License |
|[Apache Log4j](http://logging.apache.org/log4j/1.2/) | 1.2 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|Apache Log4j extras| 1.2 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[jQuery](https://jquery.org/) | 2.1.1 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[jquery.cookiebar.js](http://www.primebox.co.uk/projects/cookie-bar/) | |Creative Commons Attribution 3.0 |
|[jquery.dotdotdot.min.js](dotdotdot.frebsite.nl) | |MIT and GPL |
|[jquery.mobile.custom.min.js](dotdotdot.frebsite.nl) |1.4.5 |[MIT License](https://tldrlegal.com/license/mit-license) |
|[Materialize](http://materializecss.com)| 0.97.6 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Material Design Lite](https://github.com/google/material-design-lite)| 1.0.4|[MIT License](https://raw.githubusercontent.com/Dogfalo/materialize/master/LICENSE) |

### How do I get set up? ###

For details about configuration/installation you can see "D2.4 – WELIVE OPEN INNOVATION AND CROWDSOURCING TOOLS V2"


### Who do I talk to? ###

ENG team, Roberto Raccuglia, Filippo Giuffrida

### Copying and License

This code is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)